package com.posma.helloscp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pacroy on 9/23/17.
 */

@RestController
public class HelloScpController {
    final static Logger logger = LoggerFactory.getLogger(HelloScpController.class);
    @GetMapping("/")
    public String hello() {
        logger.info("Logging APP - Search in Default Trace SCP");
        return "Hello SCP from Spring Boot";
    }
}
