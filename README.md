# spring-boot-helloscp

Hello World Spring Boot Application for deploying on SAP Cloud Platform (Cloud Foundry and Neo environment).

# Building Instructions

## Local

```
mvn clean package install
```

## SCP Cloud Foundry Environment

```
mvn clean package install -P cf
```

## SCP Neo Environment

```
mvn clean package install -P neo
```